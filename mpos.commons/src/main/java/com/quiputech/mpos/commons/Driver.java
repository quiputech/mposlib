package com.quiputech.mpos.commons;

import com.quiputech.mpos.model.DeviceInfo;

/**
 *
 * @author giancarlo
 */
public interface Driver {

    public DeviceInfo getDeviceInfo();
    public void listBlueToothDevices();
    public void pairBlueToothDevice();
    public void injectKeyInSlot(String keyAsHex, int keyIndex);
    public void encryptDataWithKeyInSlot(String data, String algorithm, int keyIndex);
    public void readPlainCardData();
    public void readEncryptedWithKeyCardData(int keyIndex);
    public void readPin();
    public void readNumericInputFromPinpad();
    public void readEncryptedWithKeyInputFromPinpad();
    public void displayTextInVisor(String text);
    
    
}
