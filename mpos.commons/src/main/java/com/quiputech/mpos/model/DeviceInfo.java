package com.quiputech.mpos.model;

/**
 *
 * @author giancarlo
 */
public class DeviceInfo {

    private String serialNumber;
    private String cpuId;
    private String firmwareVersion;

    /**
     * @return the serialNumber
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * @param serialNumber the serialNumber to set
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * @return the cpuId
     */
    public String getCpuId() {
        return cpuId;
    }

    /**
     * @param cpuId the cpuId to set
     */
    public void setCpuId(String cpuId) {
        this.cpuId = cpuId;
    }

    /**
     * @return the firmwareVersion
     */
    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    /**
     * @param firmwareVersion the firmwareVersion to set
     */
    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }
}
